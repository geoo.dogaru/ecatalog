package com.org.ecatalog.repository;

import com.org.ecatalog.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface StudentRepository extends JpaRepository<Student, UUID> {

    @Override
    Student save(Student entity);

    @Query("select s from Student s left join fetch s.schoolClass where s.id like :id")
    Student findByIdWithSchoolClass(UUID id);
}
