package com.org.ecatalog.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(unique = true)
    private String cnp;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "school_class_id")
    private UUID schoolClassId;

    @ManyToOne(targetEntity = SchoolClass.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "school_class_id", insertable = false, updatable = false)
    private SchoolClass schoolClass;


}
