package com.org.ecatalog.service;

import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;
import com.org.ecatalog.entity.Student;
import com.org.ecatalog.mapper.StudentMapper;
import com.org.ecatalog.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private final EntityManager entityManager;

    public StudentDto create(StudentCreateDto createDto) {
        Student toBeSaved = studentMapper.toEntity(createDto);
        Student saved = studentRepository.save(toBeSaved);
        entityManager.clear();
        Student studentWithClass = studentRepository.findById(saved.getId()).get();
        return studentMapper.toDto(studentWithClass);
    }

}
