package com.org.ecatalog.controller;

import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;

import com.org.ecatalog.entity.Student;
import com.org.ecatalog.mapper.StudentMapper;
import com.org.ecatalog.repository.StudentRepository;
import com.org.ecatalog.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("students")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final StudentMapper studentMapper;
    private final StudentRepository studentRepository;

    @PostMapping
    public ResponseEntity<StudentDto> create(@RequestBody StudentCreateDto studentCreateDto) {
        return ResponseEntity.ok(studentService.create(studentCreateDto));
    }

}
